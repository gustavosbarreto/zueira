# Setupa

```$ npm install```

# Roda

```$ node app.js```

# Configuracao da sirene

Foram adicionadas as seguintes lines no crontab do servidor
para tocar a sirene as 12:00 e 18:15

```
00 12 * * * pi curl -X POST http://localhost:3000/sirene
15 18 * * * pi curl -X POST http://localhost:3000/sirene
```

# Normalizar volume de um som
```
$ ffmpeg -i input.mp3 -filter:a loudnorm output.mp3
```
