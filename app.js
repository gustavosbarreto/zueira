var express = require('express');
var app = express();
var path = require("path");
var spawn = require('child_process').spawn;

var sons = [];

app.use(express.static('static'));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, './index.html'));
});

app.post('/brasilsilsil', function(req, res) {
    sons.push(spawn('mpg123', ['static/brasilsilsil.mp3']));
    res.sendStatus(200);
});

app.post('/errou', function(req, res) {
    sons.push(spawn('mpg123', ['static/errou.mp3']));
    res.sendStatus(200);
});

app.post('/ayrton', function(req, res) {
    sons.push(spawn('mpg123', ['static/ayrton.mp3']));
    res.sendStatus(200);
});

app.post('/tanahora', function(req, res) {
    sons.push(spawn('mpg123', ['static/tanahora.mp3']));
    res.sendStatus(200);
});

app.post('/sirene', function(req, res) {
    sons.push(spawn('mpg123', ['static/sirene.mp3']));
    res.sendStatus(200);
});

app.post('/querocafe', function(req, res) {
    sons.push(spawn('mpg123', ['static/querocafe.mp3']));
    res.sendStatus(200);
});


app.post('/tapegandofogo', function(req, res) {
  sons.push(spawn('mpg123', ['static/tapegandofogo.mp3']));
  res.sendStatus(200);
});

app.post('/eagoradesliga', function(req, res) {
  sons.push(spawn('mpg123', ['static/eagoradesliga.mp3']));
  res.sendStatus(200);
});

app.post('/risadas', function(req, res) {
  sons.push(spawn('mpg123', ['static/risadas.mp3']));
  res.sendStatus(200);
});

app.post('/piao', function(req, res) {
    sons.push(spawn('mpg123', ['static/piao.mp3']));
    res.sendStatus(200);
  });

app.post('/sexta', function(req, res) {
    sons.push(spawn('mpg123', ['static/sexta.mp3']));
    res.sendStatus(200);
  });

app.post('/aplauso', function(req, res) {
    sons.push(spawn('mpg123', ['static/aplauso.mp3']));
    res.sendStatus(200);
});

app.post('/galvao', function(req, res) {
    sons.push(spawn('mpg123', ['static/galvao.mp3']));
    res.sendStatus(200);
});

app.post('/plantao', function(req, res) {
    sons.push(spawn('mpg123', ['static/plantao.mp3']));
    res.sendStatus(200);
});
// termina todos os sons que estiverem rodando
app.delete('/', function(req, res) {
    sons.forEach(function(som) {
        som.kill();
    });

    res.sendStatus(200);
});

app.listen(3000, function () {
  console.log('zueira rodando on port 3000!');
});
